# Atlassian Cloud Signup
This application is comprised of the following technologies:
- ES6
- React
- Redux
- LESS
- EsLint

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
In your terminal, in the root folder of this file, run the following commands
```
npm i
npm run dev
```
or if you are using yarn
```
yarn
yarn dev
```




