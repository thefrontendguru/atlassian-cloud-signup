import React, { Component } from 'react';
import { SignUpPage } from './pages/signUp/';
import './styles/global.less';

class App extends Component {
  render() {
    return (
      <SignUpPage />
    );
  }
}

export default App;
