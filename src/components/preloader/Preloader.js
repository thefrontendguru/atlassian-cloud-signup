/* eslint react/prop-types: 0 */
import React, { Component } from 'react';
import Spinner from '@atlaskit/spinner';

import './preloader.less';

class Preloader extends Component {
  static get propTypes() {
    return {

    };
  }

  render() {
    return (
      <div className="preloader">
        <Spinner />
        <span className="preloader--text">...loading</span>
      </div>
    );
  }
}

export default Preloader;
