import { testApi } from '../api/testLib';
/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */

export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';

export const SET_DUMMY_DATA_REQUEST = 'SET_DUMMY_DATA_REQUEST';
export const SET_DUMMY_DATA_SUCCESS = 'SET_DUMMY_DATA_SUCCESS';

export const LOAD_DUMMY_DATA_REQUEST = 'LOAD_DUMMY_DATA_REQUEST';
export const LOAD_DUMMY_DATA_SUCCESS = 'LOAD_DUMMY_DATA_SUCCESS';

export const SET_INITIAL_STATE = 'SET_INITIAL_STATE';

/**
 |--------------------------------------------------
 | Actions
 |--------------------------------------------------
 */
export const signUpUser = () => {
  return (dispatch) => {

    dispatch({ type: SIGN_UP_REQUEST });
    testApi()
      .then((data) => {
        setTimeout(() => dispatch({ type: SIGN_UP_SUCCESS, payload: data }), 2000);
      })
      .catch((err) => {
        dispatch({ type: SIGN_UP_FAILURE, payload: err });
      });
  };
};

export const setDummyData = () => {
  return (dispatch) => {
    dispatch({ type: SET_DUMMY_DATA_REQUEST });
    const mockData = { "name": "The Front End Guru", "email": "tmavu@thefrontendguru.com", "picture": "https://avatar-cdn.atlassian.com/5f124a0769e45336a22a3da895497818?by=hash", "sites": [{ "cloudId": "xxxxxxxx-34ea-4746-8501-d19b6203bb92", "displayName": "foobar", "url": "https://foobar.atlassian.net", "products": ["jira-software.ondemand"] }], "hasProduct": { "confluence": false, "jiraSoftware": true, "jiraServiceDesk": false, "jiraCore": false } };
    sessionStorage.setItem('cloud.wac.user', JSON.stringify(mockData));
    dispatch({ type: SET_DUMMY_DATA_SUCCESS, payload: mockData });
    return Promise.resolve();
  };
};

export const loadDummyData = () => {
  return (dispatch) => {
    dispatch({ type: LOAD_DUMMY_DATA_REQUEST });
    const data = JSON.parse(sessionStorage.getItem('cloud.wac.user'));
    dispatch({ type: LOAD_DUMMY_DATA_SUCCESS, payload: data });
    return Promise.resolve();
  };
};


export const clearState = () => (
  { type: SET_INITIAL_STATE }
);

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const INITIAL_STATE = {
  error: '',
  loading: false,
  loadedDummyData: null,
};

const reducer = (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case SIGN_UP_REQUEST:
      return { ...state, loading: true };
    case SIGN_UP_SUCCESS:
      return { ...state, loading: false, signUpData: action.payload };
    case SIGN_UP_FAILURE:
      return { ...state, loading: false, error: action.payload };

    case SET_DUMMY_DATA_REQUEST:
      return { ...state, loading: true };
    case SET_DUMMY_DATA_SUCCESS:
      return { ...state, loading: false, setDummyData: action.payload };

    case LOAD_DUMMY_DATA_REQUEST:
      return { ...state, loading: true };
    case LOAD_DUMMY_DATA_SUCCESS:
      return { ...state, loading: false, loadedDummyData: action.payload };

    case SET_INITIAL_STATE:
      return { ...state, ...INITIAL_STATE };

    default:
      return state;
  }
};

export default reducer;
