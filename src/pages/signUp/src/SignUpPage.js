/* eslint react/prop-types: 0 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import FieldText from '@atlaskit/field-text';
import Button from '@atlaskit/button';

import {
  Field,
  Validator
} from '@atlaskit/form';
import Preloader from '../../../components/preloader/Preloader';

import { signUpUser, setDummyData, loadDummyData } from '../../../modules/signUp-module';
import './sign-up-page.less';

class SignUpPage extends Component {
  static get propTypes() {
    return {
      // signUpUser: PropTypes.func
    };
  }

  state = {
    form: {
      siteName: '',
      firstName: '',
      lastName: '',
      email: '',
      password: ''
    }
  };

  componentDidMount() {
    // this.setAndLoadDummydata(); // This is getting the sessionstorage into the redux store
    // this.props.signUpUser(); // This is for testing APIs
    if (!!this.props.auth.loadedDummyData) {
      this.setState({
        form: {
          ...this.state,
          email: this.props.auth.loadedDummyData.email
        }
      });
    }
  }

  render() {
    const { loading } = this.props.auth;
    if (loading) return <Preloader />;

    return (
      <div className="sign-up-page">
        {this.renderHeader()}
        <div className="sign-up-page--body">
          <div className="sign-up-page--body-inner">
            {this.renderProductInfo()}
            {this.renderForm()}
          </div>
        </div>
      </div>
    );
  }

  renderHeader = () => {
    const logoImgSrc = 'https://wac-cdn.atlassian.com/dam/jcr:93075b1a-484c-4fe5-8a4f-942710e51760/Atlassian-horizontal-blue@2x-rgb.png?cdnVersion=lr';
    return (
      <header className="sign-up-page--header">
        <img className="sign-up-page--header-logo" src={logoImgSrc} />
      </header>
    );
  };

  renderProductInfo = () => {
    const productImgSrc = "https://wac-cdn.atlassian.com/dam/jcr:e806c73a-ddba-456f-9711-3b5cd91226bf/jira.svg?cdnVersion=lr";
    const productLogoImgSrc = "https://wac-cdn.atlassian.com/dam/jcr:75ba14ba-5e19-46c7-98ef-473289b982a7/Jira%20Software-blue.svg?cdnVersion=lr";
    const serverIconImgSrc = "https://wac-cdn.atlassian.com/dam/jcr:9401bf81-dd4f-4495-9f7f-0c9aa4adcf92/admin-server-tab-icon.svg?cdnVersion=lr";
    const hostServerLink = "https://www.atlassian.com/software/jira/download";

    return (
      <div className="sign-up-page--product-container">
        <div className="sign-up-page--product-image-container">
          <img className="sign-up-page--product-image" src={productImgSrc} />
        </div>
        <div className="sign-up-page--product-desc-container">
          <p>Plan, track, and release world-class software</p>
        </div>
        <div className="sign-up-page--product-logo-container">
          <img className="sign-up-page--product-logo" src={productLogoImgSrc} />
        </div>
        <div className="sign-up-page--product-help-container">
          <img className="sign-up-page--product-help-icon" src={serverIconImgSrc} />
          <p className="sign-up-page--product-help-text">
            Looking to <a href={hostServerLink}>host this on your own server</a>?
          </p>
        </div>
      </div>
    );
  };

  renderForm = () => {
    const { loadedDummyData } = this.props.auth;
    return (
      <div className="sign-up-page--form-container">
        {this.renderInputSiteName()}
        {this.renderInputFullName()}
        {this.renderInputEmail()}
        {!loadedDummyData && this.renderInputPassword()}
        {this.renderPrivacyPolicy()}
        {this.renderSubmitButton()}
      </div>
    );
  };

  renderInputSiteName = () => {
    const { siteName } = this.state.form;
    const fieldName = 'siteName';
    const configLink = 'https://my.atlassian.com/ondemand/config';
    return (
      <div className="sign-up-page--form-section wrap">
        <Field
          label="Claim yous site"
          helperText={this.getHelperText().siteNameHelperText}
          isRequired
          validators={[
            <Validator
              key
              func={this.handleValidationSiteName}
              invalid={this.getHelperText().siteNameValErrorText}
              valid={this.getHelperText().siteNameValSuccessText}
            />,
          ]}
        >
          <FieldText
            name={fieldName}
            type="text"
            shouldFitContainer
            placeholder="Your site name"
            value={siteName}
            onChange={(e) => this.handleInputChange(e, fieldName)}
          />
        </Field>
        <p className="form-link">
          <a href={configLink}>Looking to add products to an existing site?</a>
        </p>

      </div>
    );
  };

  renderInputFullName = () => {
    const { firstName, lastName } = this.state.form;
    const fieldNameFirst = 'firstName';
    const fieldNameLast = 'lastName';

    return (
      <div className="sign-up-page--form-section">
        <FieldText
          name={fieldNameFirst}
          type="text"
          label="First Name"
          required
          value={firstName}
          onChange={(e) => this.handleInputChange(e, fieldNameFirst)}
          placeholder="First" />
        <FieldText
          name={fieldNameLast}
          type="text"
          label="Last Name"
          required
          value={lastName}
          onChange={(e) => this.handleInputChange(e, fieldNameLast)}
          placeholder="Last" />
      </div>
    );
  }

  renderInputEmail = () => {
    const { email } = this.state.form;
    const { loadedDummyData } = this.props.auth;
    const fieldName = 'email';
    const emailVal = !!this.props.auth.loadedDummyData ? this.props.auth.loadedDummyData.email : email;
    const isDisabled = loadedDummyData ? true : false;
    const diffSignInLink = 'https://id.atlassian.com/logout?continue=https%3A%2F%2Fid.atlassian.com%2Flogin%3Fcontinue%3Dhttps%3A%2F%2Fwww.atlassian.com%2Ftry%2Fcloud%2Fsignup%3Fbundle%3Djira-software';

    return (
      <div className="sign-up-page--form-section wrap">
        <FieldText
          name={fieldName}
          label="Email"
          type={fieldName}
          required
          value={emailVal}
          disabled={isDisabled}
          onChange={(e) => this.handleInputChange(e, fieldName)}
          placeholder="Email" />
        {loadedDummyData &&
          <p className="form-link">
            <a href={diffSignInLink}>Sign in with a different Atlassian account</a>
          </p>
        }

      </div>
    );
  }

  renderInputPassword = () => {
    const { email } = this.state.form;
    const fieldName = 'password';

    return (
      <div className="sign-up-page--form-section wrap">
        <FieldText
          name={fieldName}
          label="Password"
          type={fieldName}
          required
          value={email}
          onChange={(e) => this.handleInputChange(e, fieldName)}
          placeholder="Password" />

      </div>
    );
  }

  renderPrivacyPolicy = () => {
    const customerAgreementLink = 'https://www.atlassian.com/legal/customer-agreement';
    const privacyPolicyLink = 'https://www.atlassian.com/legal/privacy-policy';
    return (
      <div className="sign-up-page--form-section wrap">
        <p className="form-link">
          By clicking here you accept the
            <a href={customerAgreementLink} target="_blank" rel="noopener noreferrer"> Atlassian Customer Agreement </a>
          and
            <a href={privacyPolicyLink} target="_blank" rel="noopener noreferrer"> Privacy Policy</a>.
          </p>

      </div>
    );
  }

  renderSubmitButton = () => {
    const { loadedDummyData } = this.props.auth;
    const { siteName, firstName, lastName, email, password } = this.state.form;
    // Will need a more sophisticated method of validation
    const disable = loadedDummyData ? !(!!siteName && !!firstName && !!lastName) : !(!!siteName && !!firstName && !!lastName && !!email && !!password);
    return (
      <div className="sign-up-page--form-section wrap">
        <Button
          type="submit"
          appearance="primary"
          onClick={this.handleSubmit}
          shouldFitContainer
          isDisabled={disable}
        //isLoading={loading}
        >
          Sign Up
          </Button>
      </div>
    );
  }

  handleSubmit = () => {
    this.props.signUpUser();
  };

  handleValidationSiteName = () => {
    return true;
  };

  handleInputChange = (e, fieldName) => {
    const text = e.target.value;
    this.setState({
      form: {
        ...this.state.form,
        [fieldName]: text
      }
    });
  };

  setAndLoadDummydata = () => {
    const { loadDummyData, setDummyData } = this.props;

    setDummyData()
      .then(() => loadDummyData());
  }

  getHelperText = () => ({
    siteNameHelperText: 'Must be atleast 6 characters, numbers and letters only. Once your site is provisioned, the site address can not be changed.',
    siteNameValErrorText: 'Site address must be at least 6 characters, lowercase letters and numbers only',
    siteNameValSuccessText: 'Success!',
  });
}

const mapStoreToProps = store => ({
  auth: store.auth
});

const mapActionsToProps = {
  signUpUser,
  setDummyData,
  loadDummyData
};

export default connect(mapStoreToProps, mapActionsToProps)(SignUpPage);
