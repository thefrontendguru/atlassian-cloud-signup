import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';
import auth from './modules/signUp-module';

const appReducer = combineReducers({
  auth,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

const store = createStore(
  rootReducer,
  {},
  composeWithDevTools(
    applyMiddleware(ReduxThunk),
  ));

export default store;
